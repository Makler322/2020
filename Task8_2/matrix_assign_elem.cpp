#include <iostream>
#include <stdlib.h>
//Пример реализации присваивания элементу матрицы

using namespace std;

class matrix {
    int rows;
    int cols;
    int ** m;
public:
    matrix(){
        m=new int*[3];
        for (int i=1;i<=3; i++){
            m[i-1]=new int [3];
            for (int j=1; j<=3; j++)
                m[i-1][j-1]=i+j;
        }
    }

    ~matrix(){
        for (int i=1;i<=3; i++){
            delete[]m[i-1];
        }
        delete[]m;
    }
    
    class Row {
        int cols;
        int * &r;
    public:
        Row(int row, int cols, int ** p):r(p[row-1]){
            this->cols=cols;
        }
        int & operator[](int i){
            return r[i-1];
        }
    };

    Row operator[](int i) {
        return Row(i,cols,m);
    }

    friend ostream& operator<<(ostream&s, matrix& M){
          for (int i=1; i<=3;i++){
             for(int j=1;j<=3; j++)
                cout << M.m[i-1][j-1]<<" ";
             cout << endl;
          }
          return s;
    }
};

int main(){  
    matrix M;
    cout<<M;
    double K;
    char* L;
    //L = new char;
    L = "1.5";
    
    K = 1+ atof(L);
    cout<<K;
    M[1][1]=0;
    cout <<endl;
    cout<<M;
    return 0;
}
