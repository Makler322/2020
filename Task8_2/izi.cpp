#include <iostream>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <string.h>
#include <pwd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
using namespace std;
class Cl {
int i;
public:
Cl (int x) { i = x; }
Cl (const Cl & y) { i = y.i; }
const Cl f ( const Cl & c) const { cout << c. i << endl; return *this; }
};
const Cl t1 (const Cl a) {
Cl b = Cl (5);
return b.f ( a );
}