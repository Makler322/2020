#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <cstdlib>
//
using namespace std;
#include "Matrix.h"


MatrixException::MatrixException(char* str){
    int i=0;
    while(str[i]!='\0') i++;
    error = new char [i];
    for (int j=0;j<i;j++) error[j]=str[j];
}
MatrixException::~MatrixException(){
    delete [] error;
}
void MatrixException::getError(){
    cout<<error;
    //exit(1);
}

matrix::matrix(){
    a = 0;
    b = 0;
    m = NULL;
}
matrix::matrix(const int N, const int M){
    a = N;
    b = M;
    m = new double*[a];
    for (int i = 0; i < a; i++){
        m[i] = new double [b];
        for (int j = 0; j < b; j++)
            m[i][j] = 0.0;
    }
}
matrix::matrix(const double elem){
    a = 1;
    b = 1;
    m = new double*[1];
    m[0] = new double;
    m[0][0] = elem;
}
matrix::matrix(double *S, const int ma){
    a = 1;
    b = ma;
    m = new double*[1];
    m[0] = new double [ma];
    for (int i = 0; i < ma; i++){
        //cout << S[i]<< endl;
        m[0][i] = S[i];
    }

}
matrix::matrix(const int ma, double *S){
    a = ma;
    b = 1;
    m = new double*[ma];
    for (int i = 0; i < ma; i++){
        m[i] = new double [1];
        m[i][0] = S[i];
    }

}
matrix::matrix(char *S1){
    char S[100];
    int i = 0;
    int j = 0;
    while(1){
        if ((S1[i] == '}') && (S1[i+1] == '}')){
            S[j] = S1[i];
            S[j+1] = S1[i+1];
            break;
        }
        if ((S1[i] == ',') && (S1[i+1] == '{')){
            S[j] = S1[i+1];
            j++;
            i++;i++;
        }
        else{
            S[j] = S1[i];
            j++;
            i++;
        }
        
    }
            
    a = 0;
    b = 0;
    char H[10];
    i = 1;
    int balance = 0;
    while(1){
        if (S[i] == '{'){
            a++;
            b = 0;
            while(S[i] != '}'){
                if (S[i] == ','){
                    b++;
                }
                i++;
            }
            if (S[i+1] == '}'){
                b++;
                break;
            }
            i++;
        }
    }
    //cout << a<<endl;
    //cout << b<<endl;

    m = new double*[a];
    for (int u = 0; u < a; u++){
        m[u] = new double [b];
        for (int o = 0; o < b; o++){
            m[u][o] = 0.0;
        }
    }

    i = 0;
    int aa = -2;
    int bb = 0;
    while(1){
        if (balance == 2){
            if (S[i] == ','){
                //cout<<aa<<" "<<bb<<" "<<H << endl;
                m[aa][bb] = atof(H);
                bb++;
                i++;
            }
            else{
                int j2 = 0;
                for (int y = 0; y < 10; y++){
                    H[y] = 0;
                }
                if (S[i] == '{'){
                    i++;
                }
                while((S[i] != ',') && (S[i] != '}')){
                    H[j2] = S[i];
                    i++;
                    j2++;
                }
            }
        }
        if (S[i] == '{'){
            aa++;
            bb = 0;
            balance++;
            if (balance == 2){
                int j = 0;
                for (int y = 0; y < 10; y++){
                    H[y] = 0;
                }
                if (S[i] == '{'){
                    i++;
                }
                if (S[i] == '{'){
                    i++;
                }
                while((S[i] != ',') && (S[i] != '}')){
                    H[j] = S[i];
                    i++;
                    j++;
                }
                
                //cout<<H<<endl;
            }
            
        }
        if (S[i] == '}'){
            
            //cout << H << endl;
            //cout<<aa<<" "<<bb<<" "<<H << endl;
            m[aa][bb] = atof(H);
            balance --;
            //cout << balance << endl;
            if (balance == 0){
                //perror("pp");
                break;
            }
            i++;
        }
        
        
    }
    //perror("77");

}
const int matrix::rows(){
    return a;
};
const int matrix::columns(){
    return b;
};
int matrix::set(int i, int j, double val){
    if (i >= a || j >= b){
        return 0;
    }
    m[i][j] = val;
    return 1;
}

matrix::~matrix(){
    for (int i = 0; i < a; i++){
        delete[]m[i];
    }
    delete[]m;
}

matrix& matrix::operator[](int k){
    if (k < a){
        double l[b];
        for (int i = 0; i < a; i++){
            if (i == k){
                for (int u = 0; u < b; u++){
                    l[u] = m[i][u];
                }
            }
            delete[]m[i];
        }
        delete[]m;
        
        m = new double*[1];
        m[0] = new double [b];
        for (int y = 0; y < b; y++){
            m[0][y] = l[y];
        }
        a = 1;
        return *this;
    }
    else if (k < b){
        double l[b];
        for (int i = 0; i < a; i++){
            for (int j = 0; j < b; j++){
                if (j == k){
                    l[i] = m[i][j];
                }
            }
            delete[]m[i];
        }
        delete[]m;
        
        m = new double*[a];
        for (int i = 0; i < a; i++){
            m[i] = new double[1];
            m[i][0] = l[i];
        }
        b = 1;
        return *this;
    }
    else{
        perror("Операция [] неприминима, проверьте индекс");
        return *this;
    }
}
matrix& matrix::operator=(matrix& M){
    if (m != NULL){
        for (int i = 0; i < a; i++){
            delete[]m[i];
        }
        delete[]m;
    }
    m = new double*[M.a];
    for (int i = 0; i < M.a; i++){
        m[i] = new double [M.b];
        for (int j = 0; j < M.b; j++)
            m[i][j] = M.m[i][j];
    }
    a = M.a;
    b = M.b;
    return *this;
}

void operator/(matrix& M, matrix& N){
    if (M.b == N.b){
        matrix L(M.a + N.a, M.b);
        for (int i = 0; i < M.a; i++){
            for (int j = 0; j < M.b; j++){
                L.m[i][j] = M.m[i][j];
            }
        }
        for (int i = 0; i < N.a; i++){
            for (int j = 0; j < N.b; j++){
                L.m[i + M.a][j] = N.m[i][j];
            }
        }
        M = L;
    }
    else{
        perror("Операция * неприминима, проверьте размеры\n");
    }
}

void operator|(matrix& M, matrix& N){
    if (M.a == N.a){
        matrix L(M.a, M.b + N.b);
        for (int i = 0; i < L.a; i++){
            for (int j = 0; j < M.b; j++){
                L.m[i][j] = M.m[i][j];
            }
        }
        for (int i = 0; i < L.a; i++){
            for (int j = 0; j < N.b; j++){
                L.m[i][j+M.b] = N.m[i][j];
            }
        }
        M = L;
    }
    else{
        perror("Операция * неприминима, проверьте размеры\n");
    }
}

matrix& operator*(matrix& M, matrix& N){ 
    try{
        if (!(M.b == N.a)){
            char a[13]="Invalid size";
            throw MatrixException(a);
        }
        else{
            matrix L(M.a, N.b);
            for (int i = 0; i < L.a; i++){
                for (int j = 0; j < L.b; j++){
                    for (int h = 0; h < M.b; h++){
                        L.m[i][j] += M.m[i][h]*N.m[h][j];
                    }
                    
                }
            }
            M = L;
            return M;
        }
    }
    catch(int){}
    return M;
}

void operator*=(matrix& M, matrix& N){
    try{
        if (!(M.b == N.a)){
            char a[13]="Invalid size";
            throw MatrixException(a);
        }
        else{
            matrix L(M.a, N.b);
            for (int i = 0; i < L.a; i++){
                for (int j = 0; j < L.b; j++){
                    for (int h = 0; h < M.b; h++){
                        L.m[i][j] += M.m[i][h]*N.m[h][j];
                    }
                    
                }
            }
            M = L;
        }
    }
    catch(int){}
}

bool operator==(matrix& M, matrix& N){
    if ((M.a == N.a) && (M.b == N.b)){
        for (int i = 0; i < M.a; i++){
            for (int j = 0; j < M.b; j++){
                if ((M.m[i][j] - N.m[i][j] > matrix::EPS) || (M.m[i][j] - N.m[i][j] < -matrix::EPS)){
                    return false;
                };
            }
        }
        return true;
    }
    else{
       return false;
    }
}

bool operator!=(matrix& M, matrix& N){
    if ((M.a == N.a) && (M.b == N.b)){
        for (int i = 0; i < M.a; i++){
            for (int j = 0; j < M.b; j++){
                if ((M.m[i][j] - N.m[i][j] > matrix::EPS) || (M.m[i][j] - N.m[i][j] < -matrix::EPS)){
                    return true;
                };
            }
        }
        return false;
    }
    else{
       return true;
    }
}

void operator-(matrix& M){
    for (int i = 0; i < M.a; i++){
        for (int j = 0; j < M.b; j++){
            M.m[i][j] *= -1;
        }
    }
}

void operator+=(matrix& M, matrix& N){
    try{
        if (!((M.a == N.a) && (M.b == N.b))){
            char a[13]="Invalid size";
            throw MatrixException(a);
        }
        else{
            for (int i = 0; i < M.a; i++){
                for (int j = 0; j < M.b; j++){
                    M.m[i][j] += N.m[i][j];
                }
            }
        }
    }
    catch(int){}
}

matrix& operator+(matrix& M, matrix& N){
    try{
        if (!((M.a == N.a) && (M.b == N.b))){
            char a[13]="Invalid size";
            throw MatrixException(a);
        }
        else{
            for (int i = 0; i < M.a; i++){
                for (int j = 0; j < M.b; j++){
                    M.m[i][j] += N.m[i][j];
                }
            }
            return M;
        }
    }
    catch(int){}
    return M;
}

void operator-=(matrix& M, matrix& N){
    try{
        if (!((M.a == N.a) && (M.b == N.b))){ 
            char a[13]="Invalid size";
            throw MatrixException(a);
        }
        else{
            for (int i = 0; i < M.a; i++){
                for (int j = 0; j < M.b; j++){
                    M.m[i][j] -= N.m[i][j];
                }
            }
        }
    }
    catch(int){}
}

matrix& operator-(matrix& M, matrix& N){
    try{
        if (!((M.a == N.a) && (M.b == N.b))){
            char a[13]="Invalid size";
            throw MatrixException(a);
        }
        else{
            for (int i = 0; i < M.a; i++){
                for (int j = 0; j < M.b; j++){
                    M.m[i][j] -= N.m[i][j];
                }
            }
            return M;
        }
    }
    catch(int){}
    return M;
}

matrix& operator*(matrix& M, double k){
    for (int i = 0; i < M.a; i++){
        for (int j = 0; j < M.b; j++)
            M.m[i][j] *= k;
    }
    return M;
}
void operator*=(matrix& M, double k){
    for (int i = 0; i < M.a; i++){
        for (int j = 0; j < M.b; j++)
            M.m[i][j] *= k;
    }
}
ostream& operator<<(ostream&s, matrix& M){ // Вывод
    for (int i = 0; i < M.a; i++){
        for(int j = 0; j < M.b; j++){
            cout << M.m[i][j]<<" ";
        }
        cout << endl;
    }
    return s;
}
