
using namespace std;
class MatrixException{
    char* error;
public:
    MatrixException(char* str);
    ~MatrixException();
    void getError();
};

class matrix{
    double ** m;
    int a, b;
public:
    static double EPS;
    matrix();
    matrix(const int N, const int M);
    matrix(const double elem);
    matrix(double *S, const int ma);
    matrix(const int ma, double *S);
    matrix(char *S1);
    const int rows();
    const int columns();
    int set(int i, int j, double val);
    static matrix identity(int n){
        matrix L(n, n);
        for (int i = 0; i < n; i++){
            L.m[i][i] = 1;
        }
        return L;
        
    }
    static matrix diagonal(double* vals, int n){
        matrix L(n, n);
        for (int i = 0; i < n; i++){
            L.m[i][i] = vals[i];
        }
        return L;
    }
    ~matrix();
    friend void operator/(matrix& M, matrix& N);
    friend void operator|(matrix& M, matrix& N);
    friend void operator*=(matrix& M, matrix& N);
    friend matrix& operator*(matrix& M, matrix& N);
    friend bool operator==(matrix& M, matrix& N);
    friend bool operator!=(matrix& M, matrix& N);
    friend void operator-(matrix& M);
    friend matrix& operator+(matrix& M, matrix& N);
    friend void operator+=(matrix& M, matrix& N);
    friend matrix& operator-(matrix& M, matrix& N);
    friend void operator-=(matrix& M, matrix& N);
    friend matrix& operator*(matrix& M, double k);
    friend void operator*=(matrix& M, double k);
    friend ostream& operator<<(ostream&s, matrix& M);
    matrix& operator[](int k);
    matrix& operator=(matrix& M);
};
