#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
#include "Matrix.h"
double matrix::EPS = 0.01;
int main(int argc, char **argv){
    int a, flag = 0;
    matrix G;
    cout<<"Список доступных команд:\n";
    cout<<"0) Выход\n";
    cout<<"1) matrix( int n, int m ) – размера n на m из 0.0\n";
    cout<<"2) matrix( double ) – 1 на 1 с этим элементом\n";
    cout<<"3) matrix( double*, int m ) – матрица-строка из массива длины m\n";
    cout<<"4) matrix( int n, double* ) – матрица-столбец из массива длины n\n";
    cout<<"5) matrix( char*) – из строкового представления ({{5.2,6.4}{9.3,7.4}{7.4,5.2}})\n";
    cout<<"6) static matrix matrix::identity( int n ) – возвращает единичную матрицу размера n\n";
    cout<<"7) static matrix matrix::diagonal( double* vals, int n ) – возвращает диагональную матрицу размера n с заданными элементами по главной диагонали\n";
    cout<<"8) int matrix::rows() – число строк\n";
    cout<<"9) int matrix::columns() – число столбцов\n";
    cout<<"10) matrix::set( int i, int j, double val ) – присвоить значение элементу [i][j]\n";
    cout<<"11) matrix matrix::matrix[ i ] – i-я строка в виде новой матрицы, если такая строка есть – 1-й приоритет\n";
    cout<<"12) matrix matrix::matrix[ j ] – j-й столбец в виде новой матрицы, если такой столбец есть– 2-й приоритет, – иначе ошибка\n";
    cout<<"13) matrix * scalar и matrix*=scalar – умножение матрицы на скаляр\n";
    cout<<"14) перегрузка операции << – вывод матрицы, в привычном двумерном виде\n";
    cout<<"15) matrix + matrix\n";
    cout<<"16) matrix += matrix\n";
    cout<<"17) matrix - matrix\n";
    cout<<"18) matrix -= matrix\n";
    cout<<"19) matrix * matrix\n";
    cout<<"20) matrix *= matrix\n";
    cout<<"21) -matrix – унарный минус, применить ко всем элементам\n";
    cout<<"22) matrix == matrix – точность сравнения задана статической константой matrix::EPS\n";
    cout<<"23) matrix != matrix\n";
    cout<<"24) matrix | matrix – конкатенировать (приписать) матрицы вертикально (вторую справа от первой)\n";
    cout<<"25) matrix / matrix – конкатенировать (приписать) матрицы горизонтально (вторую под первой)\n";
    cout<<"_________________________________________________________________________________________________________________________\n";
    cout << endl;
    
    while (1){
        cout<<"\nВведите номер команды:\n";
        try{
                cin >> a;
        }
        catch(MatrixException &exception) {exception.getError();}
        
        if (a == 0){
            return 0;
        }
        if (a == 1){
            flag = 1;
            int n, m;
            cout<<"Введите 2 числа - размеры матрицы\n";
            cin>>n>>m;
            matrix O(n, m);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
            
        }
        if (a == 2){
            flag = 1;
            double n;
            cout<<"Введите 1 числo\n";
            cin>>n;
            matrix O(n);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
        }
        if (a == 3){
            flag = 1;
            int m;
            cout<<"Введите 1 число int\n";
            cin>>m;
            cout<<"Введите массив из "<<m<<" числел\n";
            double N[m];
            for (int i = 0; i < m; i++){
                cin>>N[i];
            }
            matrix O(N, m);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
        }
        if (a == 4){
            flag = 1;
            int m;
            cout<<"Введите 1 число int\n";
            cin>>m;
            cout<<"Введите массив из "<<m<<" числел\n";
            double N[m];
            for (int i = 0; i < m; i++){
                cin>>N[i];
            }
            matrix O(m, N);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
        }
        if (a == 5){
            flag = 1;
            cout<<"Введите строковое представление, например {{5.2,6.4}{9.3,7.4}{7.4,5.2}}\n";
            //cout<<"ВНИМАНИЕ! Моё строковое представление немножко отличается, см. выше\n";
            char IO[100];
            scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
            matrix O(IO);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
        }
        if (a == 6){
            flag = 1;
            cout<<"Введите 1 число - размер диагональной матрицы\n";
            int n;
            cin>>n;
            matrix O = matrix::identity(n);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
        }
        if (a == 7){
            flag = 1;
            cout<<"Введите 1 число (int n) - размер диагональной матрицы\n";
            cout<<"Затем введите n чисел\n";
            int n;
            cin>>n;
            double P[n];
            for (int i = 0; i < n; i++) cin>>P[i];
            matrix O = matrix::diagonal(P, n);
            G = O;
            cout<<"Ваша матрица:\n"<<G;
        }
        if (a == 8){
            if (flag){
                cout<<G.rows()<< endl;
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            }
        }
        if (a == 9){
            if (flag){
                cout<<G.columns()<< endl;
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            }
        }
        if (a == 10){
            if (flag){
                int f, g;
                double k;
                cout<<"Введите 2 числа (координаты) и 1 число (значение)\n";
                cin>>f>>g>>k;
                if (G.set(f-1, g-1, k)){
                    cout<<G;
                }
                else{
                    cout<<"Некорректный ввод, проверьте границы\n";
                }
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            }
            
        }
        if (a == 11){
            if (flag){
                int i;
                cout<<"В пунктах 11-12 действует негласное правило (см. выше)\n";
                cout<<"Поэтому выбор функции произойдёт автоматически\n";
                cout<<"Введите 1 число, номер строки/столбца\n";
                cin>>i;
                cout<<G[i-1];
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            }
        }
        if (a == 12){
            if (flag){
                int i;
                cout<<"В пунктах 11-12 действует негласное правило (см. выше)\n";
                cout<<"Поэтому выбор функции произойдёт автоматически\n";
                cout<<"Введите 1 число, номер строки/столбца\n";
                cin>>i;
                cout<<G[i-1];
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            }
        }
        if (a == 13){
            if (flag){
                double f;
                cout<<"Введите 1 числo\n";
                cin>>f;
                cout<<"Операция *\n";
                cout<<G*f;
                cout<<"Операция *=\n";
                G*=f;
                cout<<G;
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            } 
        }
        if (a == 14){
            if (flag){
                cout<<G;
            }
            else{
                cout<<"Сначала введите матрицу (команды 1-7)\n";
            }
        }
        if (a == 15){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{2,3,1}{0,0,0}{4.7,6.5,9}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[28] = "{{2,3,1}{0,0,0}{4.7,6.5,9}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                cout<<M+N;
            }
            catch(MatrixException &exception) {exception.getError();}
            
        }
        if (a == 16){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{2,3,1}{0,0,0}{4.7,6.5,9}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[28] = "{{2,3,1}{0,0,0}{4.7,6.5,9}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                M += N;
                cout<<M;
            }
            catch(MatrixException &exception) {exception.getError();}
            
        }
        if (a == 17){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{2,3,1}{0,0,0}{4.7,6.5,9}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[28] = "{{2,3,1}{0,0,0}{4.7,6.5,9}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                cout<<M-N;
            }
            catch(MatrixException &exception) {exception.getError();}
            
        }
        if (a == 18){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{2,3,1}{0,0,0}{4.7,6.5,9}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[28] = "{{2,3,1}{0,0,0}{4.7,6.5,9}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                M-=N;
                cout<<M;
            }
            catch(MatrixException &exception) {exception.getError();}
        }
        if (a == 19){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{2,3}{1,1}{1.5,4}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[20] = "{{2,3}{1,1}{1.5,4}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                cout<<M*N;
            }
            catch(MatrixException &exception) {exception.getError();}
        }
        if (a == 20){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{2,3}{1,1}{1.5,4}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[20] = "{{2,3}{1,1}{1.5,4}}";
                matrix N1(IO);
                N = N1;
            }
            
            try{
                M*=N;
                cout<<M;
            }
            catch(MatrixException &exception) {exception.getError();}
        }
        if (a == 21){
            int i;
            cout<<"Ввод матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            try{
                -M;
                cout<<M;
            }
            catch(MatrixException &exception) {exception.getError();}

        }
        if (a == 22){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[41] = "{{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                if (M == N){
                cout<<"Матрицы равны\n";
                }
                else{
                    cout<<"матрицы не равны\n";
                }
            }
            catch(MatrixException &exception) {exception.getError();}
            
        }
        if (a == 23){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[41] = "{{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                if (M != N){
                cout<<"Матрицы не равны\n";
                }
                else{
                    cout<<"матрицы равны\n";
                }
            }
            catch(MatrixException &exception) {exception.getError();}
        }
        if (a == 24){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix N1(IO);
                N = N1;
            }
            else{
                char IO[41] = "{{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix N1(IO);
                N = N1;
            }
            try{
                M|N;
                cout<<M;
            }
            catch(MatrixException &exception) {exception.getError();}
        }
        if (a == 25){
            int i;
            cout<<"Ввод 1 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            matrix M, N;
            if (i){
                char IO[100];
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                matrix M1(IO);
                M = M1;
            }
            else{
                char IO[32] = "{{1,4.2,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix M1(IO);
                M = M1;
            }
            cout<<"Ввод 2 матрицы:\n";
            cout<<"0 - Матрица по умолчанию {{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}\n";
            cout<<"1 - Своя матрица в формате{{}{}{}}\n";
            cin>>i;
            if (i){
                char IO[100];
                //perror("0");
                scanf("%s", IO);             if (IO[0] != '{') {cout << "Некоррекный ввод!"; continue;}
                //perror("1");
                matrix N1(IO);
                //perror("2");
                N = N1;
                //perror("3");
            }
            else{
                char IO[41] = "{{1.0001,4.19999,8.6}{1,0,1}{7.3,6.5,0}}";
                matrix N1(IO);
                N = N1;
            }
            
            try{
                M/N;
                cout<<M;
            }
            catch(MatrixException &exception) {exception.getError();}
        }

    }
    return 0;
}
