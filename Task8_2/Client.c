#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <string.h>
#include <pwd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#define SIZE 16
typedef struct LIST {
                        char **lst;
                        int sizelist;
                        int curlist;
                    }
        LIST;

typedef struct BUF {
                        char *buf;
                        int curbuf;
                        int sizebuf;
                    }
        BUF;

LIST *listMain;
BUF *bufMain;
char *message, *IPAdress;
unsigned short Port = 0;
int c, flagQuit, flagName, connectionDescriptor, Adress = 0;
fd_set readfds;

void start();
void acceptMessage(int connectionDescriptor);
void fromListToCmd(int i);
void clearlist();
void nulllist();
void termlist();
void nullbuf();
void addsym();
void addword();
void word();
void newline();
void stop();



void start() { // Суть данной функции - распарсить введённые данные в список слов
    
    if(c==' '|| c=='\t') { 
        c=getchar();
        start();
    }
    else if (c=='\n' || c==EOF || c=='\r') {
        if (flagName){
            flagName = 0;
            termlist();
            free(message);
            message = NULL;
            fromListToCmd(0); // А вот здесь уже работа со словами
            struct sockaddr_in sa;
            memset(&sa, '0', sizeof(sa));
            sa.sin_family = AF_INET;
            if (Port){
                sa.sin_port = htons(Port);
            }
            else{
                //perror("12");
                sa.sin_port = htons(5000);
            }
            if (IPAdress){
                //write(1, IPAdress, strlen(IPAdress));
                if (inet_pton(AF_INET, IPAdress, &sa.sin_addr)<=0){
                    perror("Error");
                    exit(1);
                }
            }
            else{
                //perror("13");
                if (inet_pton(AF_INET, "127.0.0.1", &sa.sin_addr)<=0){
                    perror("Error");
                    exit(1);
                }
            }
            
    
            

            if (connect(connectionDescriptor, (struct sockaddr *)&sa, sizeof(sa)) < 0){
                perror("Сервер не активен");
                exit(1);
            }
            fd_set readfds;
            FD_ZERO(&readfds); 
            FD_SET(connectionDescriptor, &readfds);
            
           
            if (message == NULL){
                write(connectionDescriptor, "\n", 1);
            }
            else{
         
                write(connectionDescriptor, message, strlen(message));
            }
         
            clearlist();
            
            acceptMessage(connectionDescriptor);
            newline();
       
        }
        else{
            termlist();
            free(message);
            message = NULL;
            fromListToCmd(0);
            
            
            if (message == NULL){
                write(connectionDescriptor, "\n", 1);
            }
            // Эту команду я обрабатываю на клиенте, она не требует подключения к серверу
            else if (!strcmp(message,"\\help")){ 
                write(1, "\\quit - Выход из чата\n", 34);
                write(1, "\\users - Получить список пользователей, которые сейчас онлайн\n", 109);
                write(1, "\\private <nickname> <message> - Отправить приватное сообщение\n", 90);
                write(1, "\\privates - Получить список всех пользователей, с которыми вы состоите в приватных переписках\n", 165);
                write(1, "\\help - Вывести эту справку\n", 46);
            }
            else{
                write(connectionDescriptor, message, strlen(message));
            }
            clearlist();
        
            acceptMessage(connectionDescriptor);
            newline();
        }
    }
    else
    {
        nullbuf();
        addsym();
        c=getchar();
        word();
    }
}
/*------------------ */

void acceptMessage(int connectionDescriptor){
    //Принять сообщения могу либо с клавиатуры (0) либо с сервера (connectionDescriptor)
    char msd;
    FD_SET(connectionDescriptor, &readfds);
    FD_SET(0, &readfds);

    select(connectionDescriptor+1, &readfds, 0, 0, 0);

    if (FD_ISSET(connectionDescriptor, &readfds)){
        
        if (read(connectionDescriptor, &msd, sizeof(char)) != 0){
            if (!msd == '\0') {
                write(1, &msd, sizeof(char));
                acceptMessage(connectionDescriptor);
            }
            
        }
        else{
            write(1, "Сервер закончил свою работу\n", 53);
            stop();
        }
    }
    else if (FD_ISSET(0, &readfds)){
        c = getchar();
    }
}

void fromListToCmd(int i){
    while (i<(listMain->sizelist)-1){
        // Эту команду тоже обрабатываем на клиенте. Зачем загружать сервер, ему и так тяжело :)
        if ((!strcmp((listMain -> lst)[i], "\\quit")) && (i==0)){
            stop();
        }
        if ((!strcmp((listMain -> lst)[i], "\\help")) && (i==0)){
            message = realloc(message, strlen((listMain -> lst)[i])+1);
            strcpy(message, (listMain -> lst)[i]);
            break;
        }
        else {
            // В конец сообщений вставляем \n
            if (message != NULL) {
                if (i ==(listMain->sizelist)-2){
                    message = realloc(message, strlen(message)+strlen((listMain -> lst)[i])+2);
                    strcat(message, (listMain -> lst)[i]);
                    strcat(message, "\n");
                }
                else{
                    message = realloc(message, strlen(message)+strlen((listMain -> lst)[i])+2);
                    strcat(message, (listMain -> lst)[i]);
                    strcat(message, " ");
                }
                
            }
            else{
               
                if (i ==(listMain->sizelist)-2){
                    message = realloc(message, strlen((listMain -> lst)[i])+2);
                    strcpy(message, (listMain -> lst)[i]);
                    strcat(message, "\n");
                }
                else{
                    message = realloc(message, strlen((listMain -> lst)[i])+2);
                    strcpy(message, (listMain -> lst)[i]);
                    strcat(message, " ");
                }
                
            }
            i++;
        }
    }
}



/* Все функции, которые находятся снизу копируюутся из Task3 и Task5. Их суть - граматно распарсить данные в список*/
void clearlist(){
    int i;
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    if ((listMain -> lst)==NULL) return;
    for (i=0; (listMain -> lst)[i]!=NULL; i++)
        free((listMain -> lst)[i]);
    free((listMain -> lst));
    (listMain -> lst)=NULL;
}

void nullbuf(){
    (bufMain -> buf)=NULL;
    (bufMain -> sizebuf)=0;
    (bufMain -> curbuf)=0;
}


void nulllist(){
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    (listMain -> lst)=NULL;
}

void termlist(){
    if ((listMain -> lst)==NULL) return;
    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)+1)*sizeof(*(listMain -> lst)));
    (listMain -> lst)[(listMain -> curlist)]=NULL;
    (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)=(listMain -> curlist)+1)*sizeof(*(listMain -> lst)));
}

void addsym(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=SIZE);
    (bufMain -> buf)[(bufMain -> curbuf)++]=c;
}

void addword(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=1);
    (bufMain -> buf)[(bufMain -> curbuf)++]='\0';
    (bufMain -> buf)=realloc((bufMain -> buf),(bufMain -> sizebuf)=(bufMain -> curbuf));

    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst), ((listMain -> sizelist)+=SIZE)*sizeof(*(listMain -> lst))); 
    (listMain -> lst)[(listMain -> curlist)++]=(bufMain -> buf);
        
}

void word(){
    if (c != '\n' && c != '\t' && c != ' '){
        addsym();
        c=getchar();
        
        word();
    }
    else{
        addword();
        start();
    }
}

void newline(){
    clearlist();
    start();
}

void stop(){
    clearlist();
    
    free(listMain);
    free(bufMain);
    
    exit(0);
}


int main(int argc, char **argv){
    //Rtt = argv[1];
    // получаем свой сокет-дескриптор:
    if ((connectionDescriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        perror("client: socket");
        exit(1);
    }
    
    /*--------------------------------------------- */
    if (argc > 1){
        IPAdress = NULL;
        IPAdress = realloc(IPAdress, strlen(argv[1]));
        strcpy(IPAdress, argv[1]);
    }
    else{
        IPAdress = NULL;
    }
    printf("Введите порт (0 иначе): ");
    scanf("%hu", &Port);
    char g = getchar();
    printf("\nВведите свой никнейм:\n");
    
    flagName = 1;
    listMain = (LIST *)malloc(sizeof(LIST));
    bufMain = (BUF *)malloc(sizeof(BUF));
    nulllist();
    nullbuf();
    c = getchar();
    start(); //Вся работа клиента начинается с этой функции
    
    close(connectionDescriptor);
}