#ifndef UPDATE_H2
#define UPDATE_H2

#include "IterableObject.h"
#include "cstring"
#include <iostream>
#include <errno.h>
#include <stdlib.h>
#define MAXLEN 256
using namespace std;



template <class T> class List: public IterableObject{
    T *S;
public:
    int len; 
      
    bool contains(char P){
        int i = 0;
        for (i = 0; i < len; i++){
            if (S[i] == P)
                return true;
        }
        return false;
    }
    bool contains(int P){
        int i = 0;
        for (i = 0; i < len; i++){
            if (S[i] == P)
                return true;
        }
        return false;
    }
    bool contains(double P){
       int i = 0;
        for (i = 0; i < len; i++){
            if (((S[i] - P) < 0.001) && ((S[i] - P) > -0.001))
                return true;
        }
        return false;
    }
    bool contains(char *P){
        return true;
    }
    /* Constructors */
    List(){
        S = NULL;
        len = 0;
    }
    List(int a){
        S = new T[a];
        len = a;
    }
    /* Capacity */
    int size(){
        return len;
    }

    int max_size(List &L){
        return MAXLEN;
    }
    bool empty(List &L){
        if (L.len == 0){
            return true;
        }
        else{
            return false;
        }
    }

    /* Modifiers */
    void clear(List &L){
        if (!L.empty(L)){
            delete[] L.S;
            L.len = 0;
        }
    }
    void change(T k, int i){
        
        if (i < len){
            S[i] = k;
        }
        else{
            //S[0] = k;
        }
    }
    void add(T L){
        /* 
        S = (T* )realloc(S, (len+1)*sizeof(T));
        S[len] = L;
        len++;
        */
     
        T *Y;
        Y = new T[len + 1];
        for (int u = 0; u < len; u++){
            Y[u] = S[u];
        }
        Y[len] = L;
        
        delete[] S;
        len ++;
        S = new T[len];
        for (int ii = 0; ii < len; ii++){
            S[ii] = Y[ii];
        }
        delete[] Y;
        
    }
    List<T>& append(const List<T> &L){
        T *Y;
        Y = new T[this->len + L.len];
        for (int u = 0; u < this->len; u++){
            Y[u] = S[u];
        }
        
        for (int i = 0; i < L.len; i++){
            Y[this->len + i] = L.S[i];
        }
        delete[] S;
        this->len += L.len;
        S = new T[this->len];
        for (int ii = 0; ii < this->len; ii++){
            S[ii] = Y[ii];
        }
        delete[] Y;
        return *this;
    }
    List<T>& pop(){
        T *P;
        P = new T[len];
        
        if (len != 0){
            for (int t = 0; t < len; t++){
                P[t] = S[t];
            }
            delete [] S;
        }
        else{
            return *this;
        }
        len--;
        S = new T[len];
        for (int i = 0; i < len; i++){
            S[i] = P[i];
        }
        delete[] P;
        return *this;
    }
    T verni (int k){
        return S[k];
    }
    T* come (){
        return S;
    }
    /* Overloads */
    List& operator=(List &P){
        if (S != NULL){
            delete [] S;
        }
        S = new T[P.size()];
        len = P.size();
        for (int i = 0; i < P.size(); i++){
            S[i] = P.S[i];
        }
        return *this;
    }
    friend ostream& operator<<(ostream&s, List& M){
        cout << "<|";
        for (int i = 0; i < M.size(); i++){
            cout << M.S[i]<< "|";
        } 

        cout << ">" << endl;
        return s;
    }
    
    /* Destructor */
    ~List(){
        delete[] S;
        len = 0;
    }

};
#endif