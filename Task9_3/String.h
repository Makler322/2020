
#pragma once

#include "IterableObject.h"
#include <iostream>
using namespace std;

class String: public IterableObject{
    char *S;
public:

    

    virtual bool contains(char *P);

    /* Constructors */
    String();
    String(int k);
    String(char* K);

    /* Capacity */
    int size();
    char* come();
    void resize(String &L, int d, char P = ' ');
    int max_size(String &L);
    bool empty(String &L);
    void clear(String &L);

    /* Modifiers */
    String& append(const String &L);
    String& pop();
    

    /* Overloads */
    char operator[](int k);
    String& operator=(String &L);
    friend ostream& operator<<(ostream&s, String& M);

    /* Destructor */
    virtual ~String();

};
