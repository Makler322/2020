#include "StringList.h"
#include "String.h"
#include "cstring"
#include <iostream>
#define MAXLEN 256
using namespace std;


StringList::StringList(){
}
StringList::StringList(int n){
    S = new String[n];
    len = n;
}
void StringList::change(String &L, int i){
    
    if (i < len){
        
        S[i] = L;
    }
    else{
        //S[0] = L;
    }
}
bool StringList::contains(char *P){
    int i = 0;
    if (P == NULL){
        return true;
    }
    
    for (i = 0; i < len; i++){
        
        if (S[i].contains(P))
            return true;
        
    }
    return false;
}

int StringList::size(){
    return len;
}

String* StringList::come(){
    return S;
}

ostream& operator<<(ostream&s, StringList& M){
    for (int i = 0; i < M.size(); i++) cout << M.come()[i];
    cout << endl;
    return s;
}

StringList::~StringList(){
}