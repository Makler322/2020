#include <iostream>
struct B {
    virtual void f(){std::cout<<"F() from bb\n";}

};
struct A:B{
    void f(){std::cout<<"f() from AA\n";}

};

void f(B&b){
    A a;
    b.f();
    throw a;
}
int main(){
    try{
        A a; f(a);
    }
    catch(B&b) {std::cout<<"catch (B&b)\n";}
    catch(...) {std::cout<<"...\n";}
}