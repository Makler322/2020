#ifndef UPDATE_H3
#define UPDATE_H3


#include "IterableObject.h"
#include "String.h"
#include "List.h"
#include "cstring"
#include <iostream>
#define MAXLEN 256
using namespace std;


class StringList: public String, public List<String>{
    String *S;
public:
    int len;
    
    /* Constructors */
    StringList();
    StringList(int n);

    /* sss */
    bool contains(char *P);
    void change(String &L, int i);
    int size();
    String* come();
    /* Overloads */
    friend ostream& operator<<(ostream&s, StringList& M);
    /* Destructor */
    ~StringList();
    

};
#endif