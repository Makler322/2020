
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include "String.h"
#define MAXLEN 256


using namespace std;
String::String(): IterableObject(0){
    S = NULL;
};



String::String(int k): IterableObject(k){
    S = new char[k];
    for (int i = 0; i < k; i++){
        S[i] = '-';
    }
};

String::String(char *K): IterableObject(strlen(K)){
    int k = strlen(K);
    S = new char[k];
    for (int i = 0; i < k; i++){
        S[i] = K[i];
    }
};

char* String::come(){
    return S;
}

bool String::contains(char *P){
    int i = 0, j = 0, flag = 0;
    if (P == NULL){
        return true;
    }
    for (i = 0; i < len; i++){
        if (flag){
            if (S[i] == P[j]){
                j++;
                if (P[j] == '\0'){
                    return true;
                }
            }
            else{
                flag = 0;
                j = 0;
            }
        }
        else{
            if (S[i] == P[j]){
                flag = 1;
                j++;
                if (P[j] == '\0'){
                    return true;
                }
            }
            else{
                j = 0;
                flag = 0;
            }
        }
    }
    return false;
}

int String::size(){
    return len;
}

int String::max_size(String &L){
    return MAXLEN;
}

void String::resize(String &L, int d, char P){
    int A = d - L.size();
    char *Y;
    Y = new char[d];
    for (int u = 0; u < L.size(); u++){
        Y[u] = L.S[u];
    }
    if (A > 0){
        for (int i = 0; i < A; i++){
            Y[d-A + i] = P;
        }
    }
    delete[] L.S;
    L.S = new char[d];
    L.len = d;
    for (int u = 0; u < d; u++){
        L.S[u] = Y[u];
    }
    delete[] Y;
}

bool String::empty(String &L){
    if (L.len == 0){
        return true;
    }
    else{
        return false;
    }
}

void String::clear(String &L){
    if (!L.empty(L)){
        delete[] L.S;
        L.len = 0;
    }
}

String& String::append(const String &L){
    char *Y;
    Y = new char[this->len + L.len];
    for (int u = 0; u < this->len; u++){
        Y[u] = S[u];
    }
    
    for (int i = 0; i < L.len; i++){
        Y[this->len + i] = L.S[i];
    }
    delete[] S;
    this->len += L.len;
    S = new char[this->len];
    for (int ii = 0; ii < this->len; ii++){
        S[ii] = Y[ii];
    }
    delete[] Y;
    return *this;
}

String& String::pop(){
    char *P;
    P = new char[len];
    if (S != NULL){
        for (int t = 0; t < len; t++){
            P[t] = S[t];
        }
        delete[] S;
    }
    len--;
    S = new char[len];
    for (int i = 0; i < len; i++){
        S[i] = P[i];
    }
    delete[] P;
    return *this;
}

String& String::operator=(String &P){
    if (S != NULL){
        delete [] S;
    }
    S = new char[P.size()];
    len = P.size();
    for (int i = 0; i < P.size(); i++){
        S[i] = P.S[i];
    }
    return *this;
}

ostream& operator<<(ostream&s, String& M){ // Вывод
    if (M.S == NULL){
        cout << "<empty>";
    }
    else{
        cout << "<";
        for (int i = 0; i < M.size(); i++) cout << M.S[i];
        cout << ">";
    }
    //cout << endl;
    return s;
}

char String::operator[](int k){
    if (k < len){
        return S[k];
    }
    else{
        return S[0];
    }
    
}

String::~String(){
    delete[] S;
    len = 0;
};