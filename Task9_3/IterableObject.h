#pragma once
using namespace std;
class IterableObject{
protected:
    int len;
public:
    IterableObject();
    IterableObject(int a);
    virtual bool contains(char *S) = 0;
    virtual ~IterableObject();
};
