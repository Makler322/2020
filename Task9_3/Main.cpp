#include <fstream>
#include <iostream>
#include "StringList.h"
using namespace std;
int main(int argc, char **argv){
    
    /* Вывод по красоте */   
    ifstream in("Menu.txt");
    char str[255];
    while(in) {
        in.getline(str, 255);  // delim defaults to '\n'
        if(in) cout << str << endl;
    }
    in.close();
    /*-----------------*/
    int a;
    String O(0);
    List<int> Pi(0);
    List<char> Pc(0);
    List<double> Pd(0);
    StringList OQ;
    int listType = 1;
    while (1){
        cout << "\nВведите номер операции:\n";
        cin >> a;
        if (a == 0){
            return 0;
        }
        if (a == 1){
            int n;
            cout << "Введите n - размер строки\n";
            cin >> n;
            String L(n);
            O = L;
            cout << O;
        }
        if (a == 2){
            cout << "Введите строку\n";
            char N[256];
            cin >> N;
            String L(N);
            O = L;
            cout << O;
        }
        if (a == 3){
           cout << O.size() << endl; 
        }
        if (a == 4){
            int d;
            cout << "Введите новый размер\n";
            cin >> d;
            cout << "Введите символ Р\n";
            char P;
            cin >> P;
            O.resize(O, d, P);
            cout << O;
        }
        if (a == 5){
            cout << O.max_size(O) << endl;
        }
        if (a == 6){
            if (O.empty(O)){
                cout << "Строка пустая\n";
            }
            else{
                cout << "Строка не пустая\n";
            }
        }
        if (a == 7){
            cout << "Было: " << O << endl;
            O.clear(O);
            cout << "Стало: " << O << endl;
        }
        if (a == 8){
            cout << "Введите строку, которую хотите добавить к текущей\n";
            char N[256];
            scanf("%s", N);
            String L(N);
            O.append(L);
            cout << O;
        }
        if (a == 9){
            cout << "Было: " << O << endl;
            O.pop();
            cout << "Стало: " << O << endl;
        }
        if (a == 10){
            cout << "Введите строку, которую хотите проверить с текущей\n";
            char N[256];
            scanf("%s", N);
            if (O.contains(N)){
                cout << "Строка содержится в данной";
            }
            else{
                cout << "Строка не содержится в данной";
            }
        }
        if (a == 11){
            int i, s;
            cout << "Укажите тип списка: 1 - int, 2 - char, 3 - double\n";
            cin >> i;
            if ((i > 0) && (i < 4)){
                listType = i;
            }
            cout << "Укажите int - размер списка\n";
            cin >> s;
            if (i == 1){
                List<int> Pi1(s);
                Pi = Pi1;
                for (int j = 0; j < s; j++) Pi.change(j, j);
                cout << Pi;
            }
            if (i == 2){
                List<char> Pc1(s);
                Pc = Pc1;
                for (int j = 0; j < s; j++) Pc.change('a', j);
                cout << Pc;
            }
            if (i == 3){
                List<double> Pd1(s);
                Pd = Pd1;
                for (int j = 0; j < s; j++) Pd.change(j+0.1, j);
                cout << Pd;
            }
        }
        if (a == 12){
            if (listType == 1){
                cout << Pi.size()<<endl;
            }
            if (listType == 2){
                cout << Pc.size()<<endl;
            }
            if (listType == 3){
                cout << Pd.size()<<endl;
            }
        }
        if (a == 13){
            if (listType == 1){
                cout << Pi.max_size(Pi)<<endl;
            }
            if (listType == 2){
                cout << Pc.max_size(Pc)<<endl;
            }
            if (listType == 3){
                cout << Pd.max_size(Pd)<<endl;
            }
        }
        if (a == 14){
            if (listType == 1){
                if (Pi.empty(Pi)) cout << "Список пустой\n";
                else cout << "Список не пустой\n";
            }
            if (listType == 2){
                if (Pc.empty(Pc)) cout << "Список пустой\n";
                else cout << "Список не пустой\n";
            }
            if (listType == 3){
                if (Pd.empty(Pd)) cout << "Список пустой\n";
                else cout << "Список не пустой\n";
            }
        }
        if (a == 15){
            if (listType == 1){
                cout << "Было: " << Pi << endl;
                Pi.clear(Pi);
                cout << "Стало: " << Pi << endl;
            }
            if (listType == 2){
                cout << "Было: " << Pc << endl;
                Pc.clear(Pc);
                cout << "Стало: " << Pc << endl;
            }
            if (listType == 3){
                cout << "Было: " << Pd << endl;
                Pd.clear(Pd);
                cout << "Стало: " << Pd << endl;
            }
        }
        if (a == 16){
            int oiu;
            cout << "Введите int - длина списка\n";
            cin >> oiu;
            if (listType == 1){
                cout << "Введите "<< oiu <<" чисел через пробел\n";
                List<int> Pi1(oiu);
                int o;
                for (int i = 0; i < oiu; i++){
                    cin >> o;
                    Pi1.change(o, i);
                }
                cout << "Было: "<< Pi << endl;
                Pi.append(Pi1);
                cout << "Стало: "<< Pi << endl;
            }
            if (listType == 2){
                cout << "Введите "<< oiu <<" символов через пробел\n";
                List<char> Pc1(oiu);
                char o;
                for (int i = 0; i < oiu; i++){
                    cin >> o;
                    Pc1.change(o, i);
                }
                cout << "Было: "<< Pc << endl;
                Pc.append(Pc1);
                cout << "Стало: "<< Pc << endl;
            }
            if (listType == 3){
                cout << "Введите "<< oiu <<" чисел (double) через пробел\n";
                List<double> Pd1(oiu);
                double o;
                for (int i = 0; i < oiu; i++){
                    cin >> o;
                    Pd1.change(o, i);
                }
                cout << "Было: "<< Pd << endl;
                Pd.append(Pd1);
                cout << "Стало: "<< Pd << endl;
            }
        }
        if (a == 17){
            if (listType == 1){
                cout << "Было: "<< Pi << endl;
                Pi.pop();
                cout << "Стало: "<< Pi << endl;
            }
            if (listType == 2){
                cout << "Было: "<< Pc << endl;
                Pc.pop();
                cout << "Стало: "<< Pc << endl;
            }
            if (listType == 3){
                cout << "Было: "<< Pd << endl;
                Pd.pop();
                cout << "Стало: "<< Pd << endl;
            }
        }
        if (a == 18){
            
            if (listType == 1){
                int oiu;
                cout << "Введите число\n";
                cin >> oiu;
                if (Pi.contains(oiu)){
                    cout << "Данное число содержится в списке\n";
                }
                else{
                    cout << "Данное число не содержится в списке\n";
                }
            }
            if (listType == 2){
                char oiu;
                cout << "Введите символ\n";
                cin >> oiu;
                if (Pi.contains(oiu)){
                    cout << "Данный символ содержится в списке\n";
                }
                else{
                    cout << "Данный символ не содержится в списке\n";
                }
            }
            if (listType == 3){
                double oiu;
                cout << "Введите число (double)\n";
                cin >> oiu;
                if (Pd.contains(oiu)){
                    cout << "Данное число содержится в списке\n";
                }
                else{
                    cout << "Данное число не содержится в списке\n";
                }
            }
        }
        if (a == 19){
            int n;
            cout << "Введите размер списка\n";
            cin >> n;
            StringList K(n);
            for (int i = 0; i < n; i++){
                cout << "Введите "<< i+1 << " строку\n";
                char N[256];
                cin >> N;
                String L(N);
                //cout << L;
                K.change(L, i);
            
            }
            OQ = K;
            cout << OQ;
        }
        if (a == 20){
            cout << "Введите строку:\n";
            char N[256];
            cin >> N;
            String L(N);
            if (OQ.contains(N)){
                cout << "Содержится\n";
            }
            else{
                cout << "Не содержится\n";
            }
        }
    }
    return 0;
}