#Иерархия Классов
Реализован Вариант 2 – IterableObject

#Начало работы
Для сборки моего проекта есть Makefile. Запуск по команде:
$ make
$ ./prog

#Запуск тестов
Для работы использовал интерфейс, полностью описанный в Menu.txt
На выбор предоставляются 20 команд, которыми можно испытать все классы и взаимодействия между ними.

#От автора
Класс List сделал шаблонным.
В Menu перечислены далеко не все возможности классов, мелкие команды остались без внимания.