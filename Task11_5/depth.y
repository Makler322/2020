%{
    /* Программа вычисления глубины скобочной системы */
    #include <stdio.h>
%}
%%
    P: S { printf ( "depth: %d\n", $1 ); }
    S: '('S')'S { $$ = $2+1; if ( $$<$4 ) $$=$4; } | /*empty*/{ $$ = 0; }
%%
main () {
    printf ( "type a string, please: " );
    yyparse ();
}
yylex () {
    int c;
    c = getchar ();
    if ( c=='\n' ) return 0;
    yylval = c;
    return c;
}
yyerror ( char *s ) {
    printf ( "Depth eval: %s\n", s );
}