%{
    #include <stdio.h>
    int width=0;
%}
%%
    P: S { printf ( "width: %d\n", width ); }
    S: '('S')'S { $$=$4+1; if ( width<$$ ) width=$$; } | /*empty*/{ $$=0; }
%%
main() {
    printf ( "type a string, please: " );
    yyparse ();
}
yylex () {
    int c;
    c = getchar();
    if ( c=='\n' ) return 0;
    yylval = c;
    return c;
}
yyerror ( char *s ) {
    printf ( "Width eval: %s\n", s );
};