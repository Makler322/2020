#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>


#define TWlen 22
#define TDlen 18

using namespace std;


char * str2chr (string S);

enum type_of_lex {
    LEX_NULL,       //0 NULL
    LEX_AND,        //1 and
    LEX_BEGIN,      //2 {
    LEX_BOOL,       //3 bool
    LEX_ELSE,       //4 else
    LEX_END,        //5 }
    LEX_IF,         //6 if
    LEX_FALSE,      //7 False
    LEX_INT,        //8 int
    LEX_NOT,        //9 not
    LEX_OR,         //10 or
    LEX_PROGRAM,    //11 program
    LEX_READ,       //12 input
    LEX_TRUE,       //14 True
    LEX_VAR,        //15 var
    LEX_WHILE,      //16 while
    LEX_WRITE,      //17 print
    LEX_FIN,        //18 .
    LEX_SEMICOLON,  //20 ;
    LEX_COMMA,      //21 ,
    LEX_COLON,      //22 :
    LEX_ASSIGN,     //23 =
    LEX_LPAREN,     //24 (
    LEX_RPAREN,     //25 )
    LEX_EQ,         //26 ==
    LEX_LSS,        //27  <
    LEX_GTR,        //28 >
    LEX_PLUS,       //29  +
    LEX_MINUS,      //30  -
    LEX_TIMES,      //31 *
    LEX_SLASH,      //32 /
    LEX_LEQ,        //33 <=
    LEX_NEQ,        //34 !=
    LEX_GEQ,        //35 >=
    LEX_NUM,        //36 число
    LEX_ID,         //37 идентификатор
    POLIZ_LABEL,    //38 метка
    POLIZ_ADDRESS,  //39 адресс
    POLIZ_GO,       //40 переход по метке
    POLIZ_FGO,      //41 переход по лжи
    LEX_REAL,       //42 real  (пока не реализовано)
    LEX_ROD,        //43 остаток от деления (пока не реализовано)
    LEX_STRING,     //44 string (пока не реализовано)
    LEX_LETTER,     //51 сообщение в кавычках
    LEX_UNMINUS,    //54 унарный минус (пока не реализовано)
    LEX_DOUB,       //55 double (под вопросом) 
    LEX_LABEL,      //56 метка
    POLIZ_EQ,       //57 ==
    LEX_STRWRITE,   //58 вывод того, что в кавычках
    LEX_REALWRITE,  //60 (под вопросом)
    LEX_CHAR,       //61 char
    LEX_ARRAY,      //62 array (не доделано)
    LEX_kvadratnye_skobka_levaya, //63 [
    LEX_kvadratnye_skobka_pravaya, //64 ]

};
class Lex {
    type_of_lex t_lex;
    int v_lex;
    string v_string;
    double v_real;
    string name_of_lex;
public:
    Lex ( type_of_lex t = LEX_NULL, int v = 0, string n = "");
    type_of_lex get_type ( ) const;
    int get_value ( ) ;
    double get_real ();
    string get_string();
    friend ostream& operator << (ostream & s, Lex l );
};

class Ident {
    string name;
    bool declare;
    type_of_lex type;
    bool assign;
    int value;
    string s;
    double d;
public:
    Ident ();
    Ident (const string nam);
    bool operator== (const string& s) const {
        return name == s; 
    }
    string get_name ();
    bool get_declare ();
    void put_declare ();
    type_of_lex get_type ();
    void put_type (type_of_lex t);
    bool get_assign ();
    void put_assign ();
    int get_value ();
    void put_value (int v);
    void put_real ( double x);
    double get_real ();
    void put_string ( string x);
    string get_string();
};
extern vector <Ident> TID;

int put (const string & buf);

class Scanner {
    FILE *fp;
    char c;
    int look (const string& buf, string * list, int n);
    void gc();
public:
    static string TW[],  TD[];
    Scanner (const string program);
    Lex get_lex();
};

