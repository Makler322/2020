#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include "LexemParser.h"
#include "SyntaxParser.h" 
#include "Executer.h"


using namespace std;

int main(int argc, char **argv){
    try {
		if (argc != 2){
			cout << "Пожалуйста, выберите файл для отладки. Наш файл Test.txt" << endl;
			exit(1);
		}
		else {
            int y = 1;
            cout << "Какой этап вы хотите отладить? (1 - лексический, 2 - синтаксический + семантический + ПОЛИЗ, 3 - Запуск программы)\n";
            cin >> y;
            //y = 5;
            if (y == 1){
                Scanner scan(argv[1]);
                Lex H = scan.get_lex();
                cout << H << endl;
                while (H.get_value() != TWlen){
                    H = scan.get_lex();
                    cout << H << endl;
                }
                cout << "Сверить значения можно в Lexem_parser.h" << endl;
                
            }
            else if (y == 2){
                Parser Par(argv[1]);
                Par.analyze();
                Par.poliz.print();
                cout << "Сверить значения можно в Lexem_parser.h" << endl;
            }

            else if (y == 3){
                Interpretator I (argv[1]);
                I.interpretation ();
            }
            
            
        	
        	return 0;
		}
    }
    catch ( char c ) {
        cout << "unexpected symbol " << c << endl;
        return 1;
    }
    catch ( Lex l ) {
        cout << "SYNTAX ERROR\n";
        cout << "unexpected lexeme";
        cout << l << endl;
        return 1;
    }
    catch ( const char *source ) {
        cout << source << endl;
        return 1;
        }
    catch (...){
        cout << "Press f to pay respect\n";
        return 1;
    }
    return 0;
}
