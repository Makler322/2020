
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include "LexemParser.h"

using namespace std;

char * str2chr (string S){
    char * D;
    D = NULL;
    int n = S.size();
    D = new char[n];
    for (int i = 0; i < n; i++){
        D[i] = S[i];
    }
    return D;
}




/*

Соглашение об используемых таблицах лексем:
TW - таблица служебных слов М-языка;
TD - таблица ограничителей М-языка;
TID - таблица идентификаторов анализируемой программы;

*/


Lex::Lex ( type_of_lex t, int v, string n) {
    t_lex = t;
    v_lex = v;
    name_of_lex = n;
}
type_of_lex Lex::get_type ( ) const {
    return t_lex;
}
int Lex::get_value () {
    return v_lex;
}
double Lex::get_real () {
    return v_real;
}

string Lex::get_string(){
    return name_of_lex;
}

ostream& operator << (ostream & s, Lex l ) {
    s << '(' << l.t_lex << ',' << l.v_lex << ',' << l.name_of_lex << ");" ;
    return s;
}



string Scanner:: TW [] = {  // ВАЖНО с NULL
    "NULL", "and","{","bool","else","}",
    "if","False","int","not","or","program","input",
    "True","var","while", "print", ".", "#", "char", "array", "string" 
};

string Scanner:: TD [] = {
    "\"", ";", ",", ":", "=", "(", ")",
    "==","<", ">", "+", "-", "*", "/", "<=", ">=", "[", "]"
};

vector <Ident> TID;

Ident::Ident () { 
    declare = false; 
    assign = false; 
}
Ident::Ident (const string nam) {
    name = nam; 
    declare = false; 
    assign = false; 
}
string Ident::get_name () { 
    return name; 
}
bool Ident::get_declare () { 
    return declare; 
}
void Ident::put_declare () { 
    declare = true; 
}
type_of_lex Ident::get_type () { 
    return type; 
}
void Ident::put_type (type_of_lex t) { 
    type = t; 
}
bool Ident::get_assign () { 
    return assign; 
}
void Ident::put_assign () { 
    assign = true; 
}
int Ident::get_value () { 
    return value; 
}
void Ident::put_value (int v) { 
    value = v; 
}
void Ident::put_real ( double x) {
        d = x;
    }
double Ident::get_real () {
    return d;
}
void Ident::put_string ( string x) {
    s = x;
}
string Ident::get_string() {
    return s;
}

int put (const string & buf) {
    vector <Ident> :: iterator k;
    k = find(TID.begin(), TID.end(), buf);
    if  (k != TID.end())
        return k - TID.begin();
    TID.push_back(Ident(buf));
    return TID.size() - 1;
}

int Scanner::look (const string& buf, string * list, int n) {
    int i = 0;
    while (i < n) {
        if (buf == list [i]){
            return i;
        }
        i++; 
    }
    return 0; 
}
void Scanner::gc() { 
    c = fgetc (fp); 
}

Scanner::Scanner (const string program) {
    fp = fopen(str2chr(program), "r");
}

Lex Scanner::get_lex() { 
    enum state { 
        H, 
        IDENT, // Идентификатор (переменная)
        NUMB, // Число
        COM, // Комментарий 
        ALE, // < > = 
        NEQ, // ! =
        LETTER, // "letter"
        // Little Big
    };
    state CS = H; 
    string buf; 
    int d, j, flag_uno = 1; 
    
    do { 
        gc (); 
        switch(CS){
            case H:
                if ( c == ' ' || c == '\n' || c == '\r' || c == '\t' ) {}
                else if ( isalpha(c) || c == '{' || c == '}') { 
                    buf.push_back(c); 
                    CS = IDENT; 
                }
                else if ( isdigit (c) ) { 
                    d = c - '0'; 
                    CS = NUMB; 
                }
                else if ( c == '"' ) { 
                    CS = LETTER;
                }
                else if ( c == '-' ) { 
                    flag_uno = -1;
                    j = look ( buf, TD, TDlen);
                    return Lex ( LEX_MINUS, j, "-");
                    break;
                }
                else if ( c == '#' ) { 
                    CS = COM; 
                }
                else if ( c == '=' || c == '<' || c == '>') {
                    buf.push_back(c); 
                    CS = ALE; 
                }
                else if ( c == '.') {
                    return Lex (LEX_FIN, 17, ".");
                    break;
                }
                else if ( c == '!' ) {
                    buf.push_back(c); 
                    CS = NEQ;
                }
                else { 
                    buf.push_back(c);
                    if ( (j = look ( buf, TD, TDlen)) ){
                        if (buf == "["){
                            return Lex ((type_of_lex) LEX_kvadratnye_skobka_levaya , j, buf );
                        }
                        if (buf == "]"){
                            return Lex ((type_of_lex) LEX_kvadratnye_skobka_pravaya , j, buf );
                        }
                        return Lex ( (type_of_lex) (j + (int) LEX_FIN), j, buf );
                    }

                    else throw c;
                }
                break;

            case IDENT:
                if ( isalpha(c) || isdigit(c) ) {
                    buf.push_back(c); 
                }
                else { 
                    ungetc(c, fp);
                    j = look (buf, TW, TWlen);
                    if (j){
                        if (buf == "array"){
                            return Lex ((type_of_lex) LEX_ARRAY , j, buf );
                        }
                        if (buf == "{"){
                            return Lex ((type_of_lex) LEX_BEGIN , j, buf );
                        }
                        if (buf == "}"){
                            return Lex ((type_of_lex) LEX_END , j, buf );
                        }
                        if (buf == "char"){
                            return Lex ((type_of_lex) LEX_CHAR , j, buf );
                        }
                        if (buf == "string"){
                            return Lex ((type_of_lex) LEX_LETTER , j, buf );
                        }
                        if (buf == "var"){
                            return Lex ((type_of_lex) LEX_VAR , j, buf );
                        }
                        if (buf == "int"){
                            return Lex ((type_of_lex) LEX_INT , j, buf );
                        }
                        
                        return Lex ((type_of_lex) j , j, buf );
                    }
                    else {
                        j = put (buf);
                        return Lex (LEX_ID, j, buf);
                    }
                }
                break;

            case NUMB:
                if ( isdigit (c) ) {
                    d = d * 10 + (c - '0'); 
                }
                else { 
                    ungetc(c, fp);
                    d *= flag_uno;
                    flag_uno = 1;
                    return Lex ( LEX_NUM, d); 
                }
                break;
            case COM:
                if ( c == '\n' ) { 
                    CS = H; 
                }
                else if (c == '.' || c == '#' ) throw c;
                break;
            case ALE:
                if ( c == '=' ) {
                    buf.push_back(c);
                    j = look ( buf, TD, TDlen );
                    return Lex ((type_of_lex) ( j + (int) LEX_FIN), j, buf );
                }
                else {
                    ungetc(c, fp);
                    j = look (buf, TD, TDlen);
                    return Lex ((type_of_lex) ( j + (int) LEX_FIN), j, buf ); 
                }
                break;
            case NEQ:
                if ( c == '=' ) {
                    buf.push_back(c); 
                    j = look ( buf, TD, TDlen );
                    return Lex ( LEX_NEQ, j, buf ); 
                }
                else throw '!';
                break;
            case LETTER:
                if ( c == '"' ) { 
                    CS = H;
                    j = put(buf);
                    return Lex ((type_of_lex) ( LEX_LETTER), j , buf );
                }
                else {
                    buf.push_back(c); 
                }
                break;
        } //end switch
    } 
    while ( true );
    

};

