#include "Executer.h"
#include <stack>
#include <vector>

using namespace std;

template <typename T, typename T_EL> void from_st(T t, T_EL & x){
    x = t.top();
    t.pop();
}

void Executer::execute (Poliz& poliz ) {
    Lex pc_el;
    stack <double> args;
    stack <string> str_args;
    string lyapota;
    string krasota;
    double i, j;
    int  index = 0, size = poliz.size();
    while (index < size) {
        pc_el = poliz[index];
        switch (pc_el.get_type()) {
            case LEX_TRUE:
            case LEX_FALSE: 
            case LEX_NUM:
            case POLIZ_ADDRESS: 
            case POLIZ_LABEL:
                args.push ( pc_el.get_value () );
                break;
            case LEX_LETTER:
                
                str_args.push( pc_el.get_string() );
                break;
            case LEX_NULL:
                break;
            case LEX_DOUB:
                args.push( pc_el.get_real() );
                break;
            case LEX_ID:{
                i = pc_el.get_value ();
                //cout << i << endl;
                
                if ( TID[i].get_assign () || ! TID[i].get_assign ()) {
                    //cout << "II\n";
                    if ( TID[i].get_type() == LEX_LETTER ) {
                        str_args.push( TID[i].get_string() );
                    }
                    else if ( TID[i].get_type() == LEX_REAL ) {
                        args.push( TID[i].get_real() );
                    }
                    else {
                        args.push ( TID[i].get_value() );
                    }
                    break;
                } 
                else {
                    cout << i;
                    throw "POLIZ: Неопределённый идентификатор";
                }
                    
            }
            case LEX_NOT:
                from_st (args, i);
                args.pop();
                args.push (!i); break;
            case LEX_OR:
                from_st (args, i);
                args.pop();
                from_st (args, j);
                args.pop();
                args.push (i || j); break;
            case LEX_AND:
                from_st (args, i);
                args.pop();
                from_st (args, j);
                args.pop();
                args.push (i && j); break;
            case POLIZ_GO:
                from_st (args, i);
                args.pop(); 
                index = i - 1;
                break;
            case POLIZ_FGO: //
                from_st (args, i);
                args.pop();
                from_st (args, j);
                args.pop();
                if (!j)
                { 
                    index = i-1;
                    if (args.size() > 1)
                        args.pop();
                }
                break;
            case LEX_WRITE:{
                //perror("ww");
                if (!args.empty()){
                    from_st (args, i);
                    args.pop();
                    //perror("ww2");
                    cout << i << endl;
                    //perror("ww3");
                } 
                //args.pop(); // poka xz
                break;
            }
            case LEX_STRWRITE:{
                if (!str_args.empty()){
                    string uio;
                    from_st (str_args, uio);
                    str_args.pop();
                    //perror("dd2");
                    cout << uio << endl;
                }
                break;
            }
            case LEX_READ:{
                //cout << "ede\n";
                from_st (args, i);
                args.pop();
                int k;
                double d;
                string s;
                if ( TID[i].get_type () == LEX_INT ) {
                    cout << "Введите число (int) ";
                    cout << TID[i].get_name () << endl;
                    cin >> k;
                    TID[i].put_value(k);
                    //cout << i << "tt \n";
                    TID[i].put_assign();
                
                } 
                else if ( TID[i].get_type () == LEX_LETTER ) {
                    cout << "Введите строку (string)" << TID[i].get_name() << endl;
                    cin >> s;
                    TID[i].put_string(s);
                    TID[i].put_assign();
                } 
                else if ( TID[i].get_type() == LEX_REAL ) {
                    cout << "Введите число (real)";
                    cout << TID[i].get_name() << endl;
                    cin >> d;
                    TID[i].put_real(d);
                    TID[i].put_assign();
                }
                break;
            }
            case LEX_PLUS:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (i + j); 
                } else {
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    str_args.push (krasota + lyapota) ; 
                }
                break;
            }
            case LEX_TIMES:
                from_st (args, i);
                args.pop();
                from_st (args, j);
                args.pop();
                args.push (i * j); break;
            case LEX_MINUS:
                from_st (args, i);
                args.pop();
                from_st (args, j);
                args.pop();
                args.push (j - i); break;
            case LEX_SLASH:{
                from_st (args, i);
                args.pop();
                from_st (args, j);
                args.pop();
                if ( i ) { 
                    args.push ( j / i); 
                    break; 
                }
                else throw "POLIZ: Деление на ноль";
            }
            case LEX_EQ:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (i == j); 
                } 
                else{
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    args.push (lyapota == krasota);
                }
                break;
            }
            case LEX_GEQ:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (j >= i); 
                } 
                else{
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    args.push (krasota >= lyapota);
                }
                break;
            }
            case LEX_LEQ:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (j <= i); 
                } 
                else{
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    args.push (krasota <= lyapota);
                }
                break;
            }
            case LEX_LSS:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (j < i); 
                } 
                else{
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    args.push (krasota < lyapota);
                }
                break;
            }
            case LEX_GTR:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (j > i); 
                } 
                else{
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    args.push (krasota > lyapota);
                }
                break;
            }
            case LEX_NEQ:{
                if ( str_args.empty() ) {
                    from_st (args, i);
                    args.pop();
                    from_st (args, j);
                    args.pop();
                    args.push (j != i); 
                } 
                else{
                    from_st (str_args, lyapota);
                    str_args.pop();
                    from_st (str_args, krasota);
                    str_args.pop();
                    args.push (krasota != lyapota);
                }
                break;
            }
            case LEX_ASSIGN:{
                from_st (args, i);
                args.pop();
                if (str_args.empty()){
                    from_st (args, j);
                    args.pop();
                    if (TID[int(j)].get_type() == LEX_INT || TID[int(j)].get_type() == LEX_BOOL ) {
                        TID[int(j)].put_value(i);
                        TID[int(j)].put_assign();
                    }
                    else if (TID[int(j)].get_type() == LEX_REAL) {
                        TID[int(j)].put_real(i);
                        TID[int(j)].put_assign();
                    }
                }
                else{
                    string kol;
                    from_st (str_args, kol);
                    str_args.pop();
                    TID[int(i)].put_string(kol);
                    TID[int(i)].put_assign();
                }
                break;
            }
            default:
                cout << pc_el << endl;
                throw "POLIZ: неизвестный элемент";
        }   //end of switch

        ++index;
    }; //end of while
    cout << "Конец обработки!!!" << endl;
}

void Interpretator :: interpretation ( ) {
    pars.analyze ( );
    E.execute ( pars.poliz );
}